package ciptateknologi.arsa.routeoptimizer.activities

import android.content.*
import android.location.Location
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.KeyEvent
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.LinearLayoutManager
import id.ac.poltek_kediri.informatika.travellingbestroute.DBOpenHelper
import kotlinx.android.synthetic.main.activity_main.*
import mumayank.com.airlocationlibrary.AirLocation
import ciptateknologi.arsa.routeoptimizer.GloVar.Companion.arrayLocations
import ciptateknologi.arsa.routeoptimizer.GloVar.Companion.arraySelectedLocations
import ciptateknologi.arsa.routeoptimizer.GloVar.Companion.cv
import ciptateknologi.arsa.routeoptimizer.GloVar.Companion.db
import ciptateknologi.arsa.routeoptimizer.GloVar.Companion.lat
import ciptateknologi.arsa.routeoptimizer.GloVar.Companion.lng
import ciptateknologi.arsa.routeoptimizer.GloVar.Companion.scale
import ciptateknologi.arsa.routeoptimizer.R
import ciptateknologi.arsa.routeoptimizer.adapters.CustomAdapter
import ciptateknologi.arsa.routeoptimizer.tools.DataRequestFromGoogle
import ciptateknologi.arsa.routeoptimizer.tools.MenuHelper

class MainActivity : AppCompatActivity(), View.OnClickListener {

    lateinit var dataRequestFromGoogle: DataRequestFromGoogle
    lateinit var adapter: CustomAdapter
    lateinit var helperPop : MenuHelper


    val broadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            val action = intent?.action
            if(action.equals("places_ready")){
                Toast.makeText(this@MainActivity,"${arrayLocations.size}", Toast.LENGTH_SHORT).show()
                adapter.notifyDataSetChanged()
            }
        }
    }

    val airLocation = AirLocation(this, object : AirLocation.Callback{
        override fun onFailure(locationFailedEnum: AirLocation.LocationFailedEnum) {
            txLat.setText("Lat : --"); txLng.setText("Long : --")
        }

        override fun onSuccess(locations: ArrayList<Location>) {
            for (location in locations){
                lat = location.latitude
                lng = location.longitude
                txLat.setText("Lat : ${lat}")
                txLng.setText("Long : ${lng}")
            }
        }

    },true,1000,
        "Android tidak mengizinkan untuk mengakses GPS")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btAddToVisitingList.setOnClickListener(this)
        btVisitingList.setOnClickListener(this)
        btSearchPlaces.setOnClickListener(this)
        txCurrentPos.setOnClickListener(this)
        floatingActionButton.setOnClickListener(this)

        registerReceiver(broadcastReceiver, IntentFilter("places_ready"))
        val intent = Intent("exit_app")
        sendBroadcast(intent)

        dataRequestFromGoogle =
            DataRequestFromGoogle(this)
        db = DBOpenHelper(this).writableDatabase
        cv = ContentValues()

        adapter =
            CustomAdapter(arrayLocations)
        rcvPlaces.layoutManager = LinearLayoutManager(this)
        rcvPlaces.adapter = adapter

        //scale = resources.getDisplayMetrics().density

        airLocation.start()
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        airLocation.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        airLocation.onActivityResult(requestCode, resultCode, data)
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btAddToVisitingList -> {
                var jml = 0
                for(i in 0..(arrayLocations.size-1))
                    if(arrayLocations[i].dipilih) {
                        arraySelectedLocations.add(arrayLocations[i])
                        jml++
                    }

                //untuk menghindari pemilihan tempat yang sama
                var namaTempat =""
                var i = 0
                var deleted = 0
                while (i < arraySelectedLocations.size){
                    namaTempat = arraySelectedLocations[i].namaTempat
                    var j = i+1
                    while (j < arraySelectedLocations.size){
                        if(arraySelectedLocations[j].namaTempat.equals(namaTempat)){
                            arraySelectedLocations.removeAt(j)
                            deleted++
                        }
                        j++
                    }
                    i++
                }

                arrayLocations.clear()
                adapter.notifyDataSetChanged()
                edSearchPlace.setText("")
                Toast.makeText(this, "${jml-deleted} places added", Toast.LENGTH_LONG).show()
            }
            R.id.btVisitingList -> {
                val intent = Intent(this,VisitListActivity::class.java)
                startActivity(intent)
            }
            R.id.btSearchPlaces -> {
                airLocation.start()
                dataRequestFromGoogle.getLocationsList(edSearchPlace.text.toString().trim(), lat, lng)
            }
            R.id.txCurrentPos ->{
                txCurrentPos.setText("${clMain.rootView.height* scale}")
            }
            R.id.floatingActionButton -> {
                helperPop = MenuHelper(this,v!!, false, null)
            }
        }
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent?): Boolean {
        if(keyCode == KeyEvent.KEYCODE_BACK){
            val builder = AlertDialog.Builder(this)
            builder.setIcon(android.R.drawable.ic_dialog_info)
                .setTitle("Confirmation").setMessage("Do you want to exit ?")
                .setPositiveButton("Yes",{_,_ ->
                    val intent = Intent("exit_app")
                    sendBroadcast(intent)
                    finish()
                })
                .setNegativeButton("No",null)
                .setCancelable(false)
            builder.show()
        }
        return true
    }
}
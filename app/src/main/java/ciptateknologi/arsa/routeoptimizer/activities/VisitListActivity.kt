package ciptateknologi.arsa.routeoptimizer.activities

import android.app.AlertDialog
import android.app.ProgressDialog
import android.content.DialogInterface
import android.os.AsyncTask
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.widget.RadioGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_visit_list.*
import ciptateknologi.arsa.routeoptimizer.GloVar.Companion.arrayFinalSelectedLocations
import ciptateknologi.arsa.routeoptimizer.GloVar.Companion.arrayLocations
import ciptateknologi.arsa.routeoptimizer.GloVar.Companion.arrayMarker
import ciptateknologi.arsa.routeoptimizer.GloVar.Companion.arraySelectedLocations
import ciptateknologi.arsa.routeoptimizer.GloVar.Companion.calcMethod
import ciptateknologi.arsa.routeoptimizer.GloVar.Companion.db
//import ciptateknologi.arsa.routeoptimizer.GloVar.Companion.expectedNodesCount
import ciptateknologi.arsa.routeoptimizer.GloVar.Companion.lat
import ciptateknologi.arsa.routeoptimizer.GloVar.Companion.lng
import ciptateknologi.arsa.routeoptimizer.GloVar.Companion.nodesCount
import ciptateknologi.arsa.routeoptimizer.GloVar.Companion.nodesCounter
import ciptateknologi.arsa.routeoptimizer.GloVar.Companion.oldSearch
import ciptateknologi.arsa.routeoptimizer.GloVar.Companion.selectedDestinationCount
import ciptateknologi.arsa.routeoptimizer.GloVar.Companion.pg
import ciptateknologi.arsa.routeoptimizer.GloVar.Companion.reRequestDataFromMapsProvider
import ciptateknologi.arsa.routeoptimizer.InfoLokasi
import ciptateknologi.arsa.routeoptimizer.R
import ciptateknologi.arsa.routeoptimizer.adapters.CustomAdapter
import ciptateknologi.arsa.routeoptimizer.tools.CalcShortestRoute
import ciptateknologi.arsa.routeoptimizer.tools.DataRequestFromGoogle
import ciptateknologi.arsa.routeoptimizer.tools.MenuHelper

class VisitListActivity : AppCompatActivity(), View.OnClickListener {

    lateinit var adapter: CustomAdapter
    lateinit var helperPop : MenuHelper
    lateinit var dataRequestFromGoogle: DataRequestFromGoogle

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_visit_list)

        dataRequestFromGoogle = DataRequestFromGoogle(this)
        adapter = CustomAdapter(arraySelectedLocations)
        rcvSelectedPlaces.layoutManager = LinearLayoutManager(this)
        rcvSelectedPlaces.adapter = adapter

        btClearVisitList.setOnClickListener(this)
        btCalculateShortestRoute.setOnClickListener(this)
        floatingActionButton.setOnClickListener(this)

        pg = ProgressDialog(this)
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btClearVisitList -> {
                arraySelectedLocations.clear()
                arrayFinalSelectedLocations.clear()
                adapter.notifyDataSetChanged()
                oldSearch = ""
                reRequestDataFromMapsProvider = true
                for(i in 0 until arrayMarker.size)
                    arrayMarker[i].remove()
                arrayMarker.clear()
            }
            R.id.btCalculateShortestRoute ->{
                showChooseMethodDialog()
            }
            R.id.floatingActionButton -> {
                helperPop = MenuHelper(this,v!!,false, null)
            }
        }
    }

    inner class AsynGetDataFromMapsProvider() : AsyncTask<String, String, String>() {
        //var pg = ProgressDialog(this@VisitListActivity)

        override fun doInBackground(vararg params: String?): String {
            for (i in 0..selectedDestinationCount - 1)
                for (j in (i + 1)..selectedDestinationCount) {
                    dataRequestFromGoogle.getDistanceBetweenTwoPlacesMapbox(i, j, false, pg)
                    Thread.sleep(20)
                    dataRequestFromGoogle.getDistanceBetweenTwoPlacesMapbox(i, j, true, pg)
                    Thread.sleep(80)

                    if ((i.rem(9) == 0).and(j.rem(9) == 0)) Thread.sleep(1000)
                }
            return ""
        }

        override fun onPreExecute() {
            super.onPreExecute()
            pg.setCancelable(false)
            pg.setTitle("Please wait")
            pg.setMessage("Get data from the maps providers ...")
            pg.show()
        }

        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)
        }
    }

    fun startTheCalculation(){
        var txt = ""
        var condition = 0

        for(i in 0 until arraySelectedLocations.size){ //add final selected destinations to the array
            if(arraySelectedLocations[i].dipilih)
                txt = "${txt}${arraySelectedLocations[i].namaTempat}"
        }

        if(oldSearch == ""){ condition = 1 }
        else{ if(oldSearch != txt){ condition = 2   //baru buka aplikasi
        }else{ condition = 3   //query tidak berubah
        } }

        when(condition){
            1,2 -> {
                arrayFinalSelectedLocations.clear()     //initialize
                for(i in 0 until arraySelectedLocations.size){ //add final selected destinations to the array
                    if(arraySelectedLocations[i].dipilih)
                        arrayFinalSelectedLocations.add(arraySelectedLocations[i])
                }
                oldSearch = ""
                for(i in 0 until arrayFinalSelectedLocations.size){
                    oldSearch = "${oldSearch}${arrayFinalSelectedLocations[i].namaTempat}"
                }

                //my position selalu ada di posisi ke-1 dari arrayFinalSelectedLocations
                arrayFinalSelectedLocations.add(0, InfoLokasi("My Position", //add my_position to the list
                    "$lat , $lng",lat,lng,"",true,0))

                db.execSQL("delete from node_list") //kosongkan table untuk menampung node
                reRequestDataFromMapsProvider = true
            }
            3 -> {
                reRequestDataFromMapsProvider = false
            }
        }

        arrayLocations.clear()          //clear unused data
        selectedDestinationCount = arrayFinalSelectedLocations.size - 1
        nodesCount = 0

        for (i in 1..selectedDestinationCount) nodesCount+=i

        nodesCount *= 2
        nodesCounter = 0

        if(reRequestDataFromMapsProvider){
            val asyncGetMapData = AsynGetDataFromMapsProvider()
            asyncGetMapData.execute()
        }else{
            val calcShortestRoute = CalcShortestRoute(this)
            calcShortestRoute.calculate(selectedDestinationCount*2)
        }

    }

    fun showChooseMethodDialog(){

        var cntDipilih = 0
        for(i in 0 until arraySelectedLocations.size){
            if(arraySelectedLocations[i].dipilih) cntDipilih++
        }

        if(cntDipilih>1) {
            val builder = AlertDialog.Builder(this)
            val layoutInflater = LayoutInflater.from(this).inflate(
                R.layout.activity_pilih_metode,
                null
            )
            val rg = layoutInflater.findViewById<RadioGroup>(R.id.rg1)

            rg.check(R.id.rbDpsoGa)     //default
            val pos = rg.checkedRadioButtonId
            when (pos) {
                R.id.rbGa -> calcMethod = 0
                R.id.rbDpso -> calcMethod = 1
                R.id.rbDpsoGa -> calcMethod = 2
            }

            rg.setOnCheckedChangeListener { radioGroup, i ->
                when (radioGroup.checkedRadioButtonId) {
                    R.id.rbGa -> {
                        calcMethod = 0
                    }
                    R.id.rbDpso -> {
                        calcMethod = 1
                    }
                    R.id.rbDpsoGa -> {
                        calcMethod = 2
                    }
                }
            }

            builder.setView(layoutInflater).setTitle("Select the calculation method")
                .setPositiveButton(
                    "Proceed"
                ) { _, i -> startTheCalculation() }
                .setNegativeButton("Cancel", null)
            builder.show()
        }else{
            Toast.makeText(this,"Please select at least 2 locations",Toast.LENGTH_LONG).show()
        }
    }
}
package ciptateknologi.arsa.routeoptimizer.activities

import android.annotation.SuppressLint
import android.content.*
import android.location.Location
import android.net.Uri
import android.os.Bundle
import android.view.KeyEvent
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.LinearLayout
import android.widget.SearchView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import com.google.android.material.bottomsheet.BottomSheetBehavior
import id.ac.poltek_kediri.informatika.travellingbestroute.DBOpenHelper
import kotlinx.android.synthetic.main.bottom_sheet.*
import kotlinx.android.synthetic.main.map_layout_2.*
import mumayank.com.airlocationlibrary.AirLocation
import ciptateknologi.arsa.routeoptimizer.GloVar
import ciptateknologi.arsa.routeoptimizer.GloVar.Companion.arrayLocations
import ciptateknologi.arsa.routeoptimizer.GloVar.Companion.arrayMarker
import ciptateknologi.arsa.routeoptimizer.GloVar.Companion.arraySelectedLocations
import ciptateknologi.arsa.routeoptimizer.InfoLokasi
import ciptateknologi.arsa.routeoptimizer.R
import ciptateknologi.arsa.routeoptimizer.adapters.CustomAdapter
import ciptateknologi.arsa.routeoptimizer.adapters.CustomInfoWindowAdapter
import ciptateknologi.arsa.routeoptimizer.tools.DataRequestFromGoogle
import ciptateknologi.arsa.routeoptimizer.tools.MenuHelper

class MapActivity2 : AppCompatActivity(), OnMapReadyCallback, GoogleMap.OnMarkerClickListener,
    GoogleMap.OnInfoWindowClickListener, GoogleMap.OnPoiClickListener,
    GoogleMap.OnMapLongClickListener, SearchView.OnQueryTextListener, View.OnClickListener {

    var gMap : GoogleMap? = null
    lateinit var airLocation : AirLocation
    lateinit var smf : SupportMapFragment
    lateinit var helperPop : MenuHelper
    var locationNumber = 1
    var markerId = ""

    lateinit var dataRequestFromGoogle: DataRequestFromGoogle
    lateinit var adapter: CustomAdapter
    lateinit var btmSheetBehavior: BottomSheetBehavior<LinearLayout>

    val broadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            val action = intent?.action
            if(action.equals("places_ready")){
                //Toast.makeText(this@MapActivity2,"${GloVar.arrayLocations.size}", Toast.LENGTH_SHORT).show()
                adapter.notifyDataSetChanged()
            }
        }
    }

    override fun onMapReady(p0: GoogleMap) {
        gMap = p0
        if(gMap!=null){

            gMap!!.setOnMapLongClickListener(this)
            gMap!!.setOnMarkerClickListener(this)
            gMap!!.setInfoWindowAdapter(CustomInfoWindowAdapter(this,true))
            gMap!!.setOnInfoWindowClickListener(this)
            gMap!!.setOnPoiClickListener(this)
            fabMapType.setOnClickListener {
                MenuHelper(this,it, true,gMap!!)
            }

            airLocation = AirLocation(this, object : AirLocation.Callback{
                override fun onFailure(locationFailedEnum: AirLocation.LocationFailedEnum) {
                }
                override fun onSuccess(locations: ArrayList<Location>) {
                    for (location in locations){
                        GloVar.lat = location.latitude
                        GloVar.lng = location.longitude
                    }
                    val ll = LatLng(GloVar.lat,GloVar.lng)
                    gMap!!.addMarker(MarkerOptions().position(ll).title("My Location")
                        .snippet("Unknown address"))
                    gMap!!.animateCamera(CameraUpdateFactory.newLatLngZoom(ll,16.0f))
                }

            },true,1000,
                "Android doesn't allow to use gps")
            airLocation.start()
        }
    }

    override fun onMarkerClick(p0: Marker): Boolean {
        markerId = p0.id
        gMap!!.animateCamera(CameraUpdateFactory.newLatLng(p0.position))
        p0.showInfoWindow()
        return true
    }

    override fun onMapLongClick(p0: LatLng) {
        if(gMap != null){
            arrayMarker.add(
                gMap!!.addMarker(MarkerOptions().position(p0).title("Location $locationNumber").
                snippet("Unknown address").
                icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_VIOLET)))!!
            )
            locationNumber++
        }
    }

    override fun onInfoWindowClick(p0: Marker) {
        val builder = AlertDialog.Builder(this)
        builder.setTitle("More option")
            .setNegativeButton("Delete marker") { dialogInterface, i ->
                var i = 0
                while (i < arrayMarker.size) {
                    if (arrayMarker[i].equals(p0)) {
                        arrayMarker[i].remove()
                        arrayMarker.removeAt(i)
                    }
                    i++
                }
            }
            .setPositiveButton("More info on marker") { dialogInterface, i ->
                var uri: Uri
                if (p0.snippet!!.contains("Unknown address"))
                    uri = Uri.parse("geo:0,0?q=${p0.position.latitude},${p0.position.longitude}")
                else
                    uri =
                        Uri.parse("geo:${p0.position.latitude},${p0.position.longitude}?q=${p0.title}")

                val intent = Intent(Intent.ACTION_VIEW, uri)
                intent.setPackage("com.google.android.apps.maps")
                startActivity(intent)
            }
        builder.show()
    }

    override fun onPoiClick(p0: PointOfInterest) {
        markerId = p0.placeId
        gMap!!.animateCamera(CameraUpdateFactory.newLatLng(p0.latLng))
        val builder = AlertDialog.Builder(this)
        builder.setTitle("${p0.name}").setMessage(
            "Lat = ${p0.latLng.latitude}\nLng = ${p0.latLng.longitude}")
            .setNegativeButton("Add Location"){dialogInterface, i ->
                arrayMarker.add(
                    gMap!!.addMarker(MarkerOptions().position(p0.latLng).title("${p0.name}").
                    snippet("Unknown address").
                    icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_VIOLET)))!!
                )
            }
            .setPositiveButton("More info on marker"){dialogInterface, i ->
                val intent = Intent(Intent.ACTION_VIEW,
                    Uri.parse("geo:${p0.latLng.latitude},${p0.latLng.longitude}?q=${p0.name}"))
                intent.setPackage("com.google.android.apps.maps")
                startActivity(intent)
            }
        builder.show()
    }

    @SuppressLint("NotifyDataSetChanged")
    override fun onClick(p0: View?) {
        when(p0!!.id){
            R.id.btnAddLocations -> {
                if(gMap != null){
                    for(i in 0 until arrayLocations.size){
                        if(arrayLocations[i].dipilih){
                            arrayMarker.add(gMap!!.addMarker(MarkerOptions().position(
                                LatLng(arrayLocations[i].lat, arrayLocations[i].lng)).
                                title(arrayLocations[i].namaTempat).snippet(arrayLocations[i].alamat).
                                icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE)))!!)
                        }
                    }
                    arrayLocations.clear()
                    adapter.notifyDataSetChanged()

                    btmSheetBehavior.state = BottomSheetBehavior.STATE_COLLAPSED
                    gMap!!.animateCamera(CameraUpdateFactory.newLatLngZoom(LatLng(GloVar.lat, GloVar.lng),10.0f))

                }
            }
            R.id.btnNext -> {
                if(arrayMarker.size>1){ // MINIMAL ADA DUA TEMPAT YANG HARUS DIPILIH
                    for(i in 0 until arrayMarker.size){
                        arraySelectedLocations.add(InfoLokasi(arrayMarker[i].title!!,arrayMarker[i].snippet!!,
                            arrayMarker[i].position.latitude,arrayMarker[i].position.longitude,"",true,0))
                    }

                    //untuk menghindari pemilihan tempat yang sama
                    var namaTempat =""
                    var i = 0
                    var deleted = 0
                    while (i < arraySelectedLocations.size){
                        namaTempat = arraySelectedLocations[i].namaTempat
                        var j = i+1
                        while (j < arraySelectedLocations.size){
                            if(arraySelectedLocations[j].namaTempat.equals(namaTempat)){
                                arraySelectedLocations.removeAt(j)
                                deleted++
                            }
                            j++
                        }
                        i++
                    }

                    adapter.notifyDataSetChanged()
                    val intent = Intent(this,VisitListActivity::class.java)
                    startActivity(intent)
                }else{
                    Toast.makeText(this,
                        "Please select at least 2 locations", Toast.LENGTH_LONG).show()
                }
            }
            R.id.btnClearLocations ->{
                for(i in 0 until arrayMarker.size)
                    arrayMarker[i].remove()
                arrayMarker.clear()
                searchView.setQuery("",false)
            }
            R.id.fabHelp ->{
                helperPop = MenuHelper(this,p0!!, false, null)
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.map_layout_2)

        //searchview
        searchView.setOnQueryTextListener(this)
        searchView.isSubmitButtonEnabled = true
        searchView.setIconified(true)

        //maps
        smf = supportFragmentManager.findFragmentById(R.id.fragmentMap) as SupportMapFragment
        smf.getMapAsync(this)

        //database
        dataRequestFromGoogle =
            DataRequestFromGoogle(this)
        GloVar.db = DBOpenHelper(this).writableDatabase
        GloVar.cv = ContentValues()

        //close splash
        registerReceiver(broadcastReceiver, IntentFilter("places_ready"))
        val intent = Intent("exit_app")
        sendBroadcast(intent)

        //recyclerview
        adapter =
            CustomAdapter(GloVar.arrayLocations)
        rcvPlaces2.layoutManager = LinearLayoutManager(this)
        rcvPlaces2.adapter = adapter

        //set bottom sheets height, width and behaviour
        val sheetParams = btmSheet.layoutParams
        sheetParams.height = (GloVar.screenHeightPx * 0.8).toInt()
        sheetParams.width = GloVar.screenWidthPx
        btmSheet.layoutParams = sheetParams
        btmSheetBehavior = BottomSheetBehavior.from(btmSheet)

        btnAddLocations.setOnClickListener(this)
        btnNext.setOnClickListener(this)
        btnClearLocations.setOnClickListener(this)
        fabHelp.setOnClickListener(this)
    }

    override fun onQueryTextSubmit(p0: String?): Boolean {
        val inputMethodManager = getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.hideSoftInputFromWindow((searchView as View).applicationWindowToken,0)
        //inputMethodManager.hideSoftInputFromWindow(),0)

        dataRequestFromGoogle.getLocationsList(p0!!.trim(), GloVar.lat, GloVar.lng)
        btmSheetBehavior.state = BottomSheetBehavior.STATE_SETTLING.or(BottomSheetBehavior.STATE_HALF_EXPANDED)

        return true
    }

    override fun onQueryTextChange(p0: String?): Boolean {
        btmSheetBehavior.state = BottomSheetBehavior.STATE_COLLAPSED
        return true
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent?): Boolean {
        if(keyCode == KeyEvent.KEYCODE_BACK){
            val builder = AlertDialog.Builder(this)
            builder.setIcon(android.R.drawable.ic_dialog_info)
                .setTitle("Confirmation").setMessage("Do you want to exit ?")
                .setPositiveButton("Yes",{_,_ ->
                    val intent = Intent("exit_app")
                    sendBroadcast(intent)
                    finish()
                })
                .setNegativeButton("No",null)
                .setCancelable(false)
            builder.show()
        }
        return true
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        airLocation.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        airLocation.onActivityResult(requestCode, resultCode, data)
    }
}
package ciptateknologi.arsa.routeoptimizer.activities

import android.content.Intent
import android.graphics.*
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import kotlinx.android.synthetic.main.map_layout.*
import ciptateknologi.arsa.routeoptimizer.GloVar.Companion.adapter
import ciptateknologi.arsa.routeoptimizer.GloVar.Companion.arrGBestFinal
import ciptateknologi.arsa.routeoptimizer.GloVar.Companion.arrayBoundary
import ciptateknologi.arsa.routeoptimizer.GloVar.Companion.arrayFinalSelectedLocations
import ciptateknologi.arsa.routeoptimizer.GloVar.Companion.arrayLocations
import ciptateknologi.arsa.routeoptimizer.GloVar.Companion.c
import ciptateknologi.arsa.routeoptimizer.GloVar.Companion.calcMethod
import ciptateknologi.arsa.routeoptimizer.GloVar.Companion.day
import ciptateknologi.arsa.routeoptimizer.GloVar.Companion.db
import ciptateknologi.arsa.routeoptimizer.GloVar.Companion.gBestDistanceFinal
import ciptateknologi.arsa.routeoptimizer.GloVar.Companion.hour
import ciptateknologi.arsa.routeoptimizer.GloVar.Companion.maxLat
import ciptateknologi.arsa.routeoptimizer.GloVar.Companion.maxLng
import ciptateknologi.arsa.routeoptimizer.GloVar.Companion.minLat
import ciptateknologi.arsa.routeoptimizer.GloVar.Companion.minLng
import ciptateknologi.arsa.routeoptimizer.GloVar.Companion.minute
import ciptateknologi.arsa.routeoptimizer.GloVar.Companion.polylineReady
import ciptateknologi.arsa.routeoptimizer.GloVar.Companion.scale
import ciptateknologi.arsa.routeoptimizer.GloVar.Companion.second
import ciptateknologi.arsa.routeoptimizer.InfoLokasi
import ciptateknologi.arsa.routeoptimizer.R
import ciptateknologi.arsa.routeoptimizer.adapters.CustomInfoWindowAdapter
import ciptateknologi.arsa.routeoptimizer.adapters.PolylinesAdapter
import ciptateknologi.arsa.routeoptimizer.tools.DecodeGoogleMapsPolyline
import ciptateknologi.arsa.routeoptimizer.tools.KMeansClustering
import ciptateknologi.arsa.routeoptimizer.tools.MenuHelper
import java.util.*
import kotlin.collections.ArrayList


class MapActivity : AppCompatActivity(), OnMapReadyCallback,GoogleMap.OnMarkerClickListener,
    GoogleMap.OnPoiClickListener, GoogleMap.OnInfoWindowClickListener {

    var gMap : GoogleMap? = null
    lateinit var mapFragment : SupportMapFragment
    lateinit var decodeGmPoly : DecodeGoogleMapsPolyline
    lateinit var ll : List<LatLng>
    lateinit var ll2 : ArrayList<LatLng>
    lateinit var llMapBox : ArrayList<LatLng>
    lateinit var helperPop : MenuHelper
    lateinit var kMeansClustering: KMeansClustering
    var txDst = ""
    var twoLoc = ""
    var points = ""
    var zoom = false
    var color = Color.MAGENTA
    var markerId = ""
    var lat = 0.0
    var lng = 0.0

    override fun onDestroy() {
        arrayLocations.clear()
        super.onDestroy()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.map_layout)

        ll = ArrayList()
        ll2 = ArrayList()
        llMapBox = ArrayList()
        decodeGmPoly = DecodeGoogleMapsPolyline()
        rcvRoute.layoutManager = LinearLayoutManager(this)
        polylineReady = false
        kMeansClustering = KMeansClustering(this)
        kMeansClustering.getMaxMinLatLng()

        fabOpenClose.setOnClickListener {
            if(cardView.visibility == View.VISIBLE) {
                cardView.visibility = View.GONE
                fabOpenClose.setImageResource(R.drawable.ic_expand_less_black_24dp)
                guideline.setGuidelinePercent(0.0f)
            }
            else{
                cardView.visibility = View.VISIBLE
                fabOpenClose.setImageResource(R.drawable.ic_expand_more_black_24dp)
                guideline.setGuidelinePercent(0.4f)
            }
        }
        floatingActionButton.setOnClickListener {
            helperPop = MenuHelper(this,it, false,null)
        }

        scale = resources.getDisplayMetrics().density

        mapFragment = supportFragmentManager.findFragmentById(R.id.fragment) as SupportMapFragment
        mapFragment.getMapAsync(this)
     }

    override fun onMapReady(p0: GoogleMap) {
        gMap = p0
        if(gMap!=null){
            gMap!!.setInfoWindowAdapter(CustomInfoWindowAdapter(this,false))
            gMap!!.setOnInfoWindowClickListener(this)
            gMap!!.setOnMarkerClickListener(this)
            gMap!!.setOnPoiClickListener(this)
            fabMapType2.setOnClickListener {
                MenuHelper(this,it, true,gMap!!)
            }

            //Tahun 2022
            //menggambar polygon rectangle
            gMap!!.addPolygon(
                PolygonOptions().addAll(arrayBoundary).fillColor(Color.TRANSPARENT).strokeColor(Color.BLUE)
            )
            kMeansClustering.calculate()

            displayDestOrder()
            adapter = PolylinesAdapter(this,arrayLocations,gMap!!)
            rcvRoute.adapter = adapter
        }
    }

    //display the order of the tourist location
    fun displayDestOrder(){
        val usingMapbox=true
        var durasi = 0.0
        arrayLocations.clear()

        for(i in 0 until arrGBestFinal.size-1){
            twoLoc="${arrGBestFinal[i]};${arrGBestFinal[i+1]}"
            c = db.rawQuery("select points, dist, duration from node_list where node=?", arrayOf(twoLoc))
            c.moveToFirst(); points = c.getString(0); durasi += c.getDouble(2)

            //arrayLocations saat ini dipakai untuk menampilkan rute yg ditempuh,
            //jadi isi field-nya mungkin tidak sesuai
            arrayLocations.add(
                InfoLokasi(
                "${i}. ${arrayFinalSelectedLocations[arrGBestFinal[i]].namaTempat} (${arrGBestFinal[i]})",
                "${c.getString(0)}",  //points
                    c.getDouble(1),0.0, //dist
                    "$twoLoc",false,0
                )
            )

            if(usingMapbox){
                val tokens = StringTokenizer(points,";",false)
                while(tokens.hasMoreTokens()){
                    lat = tokens.nextToken().toDouble()
                    lng = tokens.nextToken().toDouble()
                    llMapBox.add(LatLng(lat,lng))
                }
                drawPolyOnMap(llMapBox,Color.DKGRAY,0f,8f)
                llMapBox.clear()
            }else{
                ll = decodeGmPoly.decodePoly(points)
                drawPolyOnMap(ll,Color.DKGRAY,0f,8f)
            }

            ll2.add(LatLng(arrayFinalSelectedLocations[arrGBestFinal[i]].lat,
                arrayFinalSelectedLocations[arrGBestFinal[i]].lng))
            zoom = i.equals(0)

            drawMarkerOnMap(i,zoom)
        }
        ll2.add(LatLng(arrayFinalSelectedLocations[arrGBestFinal[0]].lat,
            arrayFinalSelectedLocations[arrGBestFinal[0]].lng))

        when(calcMethod){
            0 -> color = Color.BLUE      //GA
            1 -> color = Color.MAGENTA  //DPSO
            2 -> color = Color.RED     //DPSO-GA
        }

        drawPolyOnMap(ll2,color,1f,6f)

        val durasiL = durasi.toLong()
        day = durasiL/86400
        hour = (durasiL%86400)/3600
        minute = ((durasiL%86400)%3600)/60
        second = ((durasiL%86400)%3600)%60
        //Log.i("GA_Calc","$durasiL $day $hour $minute $second")

        //var latTxt = ""
        //for(i in 0 until arrayFinalSelectedLocations.size)
        //    latTxt += "${arrayFinalSelectedLocations[i].lat}\n"

        txOrder.setText(
            //"${latTxt}\n"+
            //"MaxLat = ${maxLat}\nMaxLng = ${maxLng}\nMinLat = ${minLat}\nMinLng = ${minLng}\n"+
            "Total Route Length : ${gBestDistanceFinal/1000} kilometer ${gBestDistanceFinal%1000} meter\n" +
                "Travel Duration : ${day} day ${hour} hour ${minute} minute ${second} second")
        arrayLocations.add(
            InfoLokasi(
                "0. ${arrayFinalSelectedLocations[arrGBestFinal[0]].namaTempat} (${arrGBestFinal[0]})",
                "",  //points
                0.0,0.0, //dist
                "",false,0
            )
        )
    }

    fun drawPolyOnMap(list : List<LatLng>, color: Int, zIndex : Float, width : Float) {
        val polyline =
            PolylineOptions().addAll(list).clickable(true).color(color).width(width).zIndex(zIndex)
        gMap?.addPolyline(polyline)
    }

    val conf = Bitmap.Config.ARGB_8888
    var bmp = Bitmap.createBitmap(50,50,conf)
    var paint = Paint(Paint.ANTI_ALIAS_FLAG)
    val bounds = Rect()
    var canvas = Canvas(bmp)

    fun createMarker(teks : String):Bitmap{
        if(teks.length.equals(2)){
            bmp = Bitmap.createBitmap(80,50,conf)
            canvas = Canvas(bmp)
        }
        paint.color = Color.RED // Text color
        paint.textSize = 20 * scale // Text size
        paint.setTypeface(Typeface.create(Typeface.DEFAULT, Typeface.BOLD));
        paint.setShadowLayer(1f, 0f, 1f, Color.WHITE) // Text shadow
        paint.getTextBounds(teks, 0, teks.length, bounds)


        canvas.drawColor(Color.TRANSPARENT, PorterDuff.Mode.MULTIPLY) //clear canvas
        canvas.drawARGB(191, 255,255,127) //paint canvas
        canvas.drawText(teks, 10f, 43f, paint)
        return bmp
    }

    fun drawMarkerOnMap(i : Int, zoom : Boolean){
        val ll = LatLng(arrayFinalSelectedLocations[arrGBestFinal[i]].lat,
            arrayFinalSelectedLocations[arrGBestFinal[i]].lng)

        if(zoom) gMap!!.animateCamera(CameraUpdateFactory.newLatLngZoom(ll,16.0f))
        gMap!!.addMarker(MarkerOptions().position(ll)
            .title(arrayFinalSelectedLocations[arrGBestFinal[i]].namaTempat)
            .snippet(arrayFinalSelectedLocations[arrGBestFinal[i]].alamat)
            .icon(BitmapDescriptorFactory.fromBitmap(createMarker("$i")))
            //.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_VIOLET))
            .anchor(0.5f,1f))
    }

    override fun onInfoWindowClick(p0: Marker) {
        var uri: Uri
        if(p0.title!!.contains("Location"))
            uri = Uri.parse("geo:0,0?q=${p0.position.latitude},${p0.position.longitude}")
        else
            uri = Uri.parse("geo:${p0.position.latitude},${p0.position.longitude}?q=${p0.title}")

        val intent = Intent(Intent.ACTION_VIEW,uri)
        intent.setPackage("com.google.android.apps.maps")
        startActivity(intent)
    }

    override fun onMarkerClick(p0: Marker): Boolean {
        markerId = p0.id
        gMap!!.animateCamera(CameraUpdateFactory.newLatLngZoom(p0.position,13.0f))
        p0.showInfoWindow()
        return true
    }

    override fun onPoiClick(p0: PointOfInterest) {
        markerId = p0.placeId
        gMap!!.animateCamera(CameraUpdateFactory.newLatLngZoom(p0.latLng,13.0f))
        val builder = AlertDialog.Builder(this)
        builder.setTitle("${p0.name}").setMessage(
            "Lat = ${p0.latLng.latitude}\nLng = ${p0.latLng.longitude}")
            .setPositiveButton("More info on marker"){dialogInterface, i ->
                val intent = Intent(Intent.ACTION_VIEW,
                    Uri.parse("geo:${p0.latLng.latitude},${p0.latLng.longitude}?q=${p0.name}"))
                intent.setPackage("com.google.android.apps.maps")
                startActivity(intent)
            }
        builder.show()
    }
}
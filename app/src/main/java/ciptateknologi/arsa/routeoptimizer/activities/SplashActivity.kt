package ciptateknologi.arsa.routeoptimizer.activities

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.os.Handler
import android.util.DisplayMetrics
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_splash.*
import ciptateknologi.arsa.routeoptimizer.GloVar
import ciptateknologi.arsa.routeoptimizer.R

class SplashActivity : AppCompatActivity() {

    lateinit var runnable: Runnable
    val broadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            val action = intent?.action
            if(action.equals("exit_app")) {
                finish()
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        registerReceiver(broadcastReceiver, IntentFilter("exit_app"))
//        runnable = Runnable {
//                val intent = Intent(this@SplashActivity,MainActivity::class.java)
//                startActivity(intent)
//        }
        runnable = Runnable {
            val intent = Intent(this@SplashActivity,MapActivity2::class.java)
            startActivity(intent)
        }
    }

    @Suppress("DEPRECATION")
    override fun onStart() {
        super.onStart()
        val dm = DisplayMetrics()
        windowManager.defaultDisplay.getRealMetrics(dm)
        GloVar.screenHeightPx = dm.heightPixels
        GloVar.screenWidthPx = dm.widthPixels

        //Toast.makeText(this, "${GloVar.screenHeightPx}, ${GloVar.screenWidthPx}", Toast.LENGTH_LONG).show()

        Handler().postDelayed(runnable,2000)
    }
}
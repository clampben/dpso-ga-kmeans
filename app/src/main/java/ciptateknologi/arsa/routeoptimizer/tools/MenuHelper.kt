package ciptateknologi.arsa.routeoptimizer.tools

import android.app.AlertDialog
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import com.github.barteksc.pdfviewer.PDFView
import com.google.android.gms.maps.GoogleMap
import com.shehabic.droppy.DroppyMenuPopup
import ciptateknologi.arsa.routeoptimizer.R

class MenuHelper(context : Context, vi : View, isMapType : Boolean, val gMap : GoogleMap?) {
    val context = context
    val vw = vi
    var droppyMenu : DroppyMenuPopup
    var droppyMenuBuilder : DroppyMenuPopup.Builder
    var menuId : Int = 0

    init {

        if(isMapType) menuId = R.menu.menu_pop_map_type
        else menuId = R.menu.menu_pop

        droppyMenuBuilder = DroppyMenuPopup.Builder(context,vw)
        droppyMenu = droppyMenuBuilder.fromMenu(menuId)
            .setOnClick { v, id ->
                when(id){
                    R.id.menuAbout -> {
                        val layoutInflater = LayoutInflater.from(context).inflate(
                            R.layout.activity_about,null)
                        val builder = AlertDialog.Builder(context)
                        builder.setView(layoutInflater).setNeutralButton("Close",null)
                        builder.show()
                    }
                    R.id.menuMan -> {
                        val layoutInflater = LayoutInflater.from(context).inflate(
                            R.layout.activity_manual,null)
                        val pdfView = layoutInflater.findViewById<PDFView>(R.id.pdfView)
                        pdfView.fromAsset("uiguide.pdf")
                            .enableSwipe(true).defaultPage(0)
                            .load()

                        val builder = AlertDialog.Builder(context)
                        builder.setView(layoutInflater).setNeutralButton("Close",null)
                        builder.show()
                    }
                    R.id.itemNormal ->      gMap!!.mapType = GoogleMap.MAP_TYPE_NORMAL
                    R.id.itemSatellite ->   gMap!!.mapType = GoogleMap.MAP_TYPE_HYBRID
                    R.id.itemTerrain ->     gMap!!.mapType = GoogleMap.MAP_TYPE_TERRAIN
                }
            }
            .build()

        droppyMenu.show()
    }
}
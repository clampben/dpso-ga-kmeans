package ciptateknologi.arsa.routeoptimizer.tools

import android.content.Context
import android.util.Log
import ciptateknologi.arsa.routeoptimizer.GloVar.Companion.arrayBoundary
import ciptateknologi.arsa.routeoptimizer.GloVar.Companion.arrayCentroidK
import ciptateknologi.arsa.routeoptimizer.GloVar.Companion.arrayCentroidKOld
import ciptateknologi.arsa.routeoptimizer.GloVar.Companion.arrayFinalSelectedLocations
import ciptateknologi.arsa.routeoptimizer.GloVar.Companion.maxLat
import ciptateknologi.arsa.routeoptimizer.GloVar.Companion.maxLng
import ciptateknologi.arsa.routeoptimizer.GloVar.Companion.minLat
import ciptateknologi.arsa.routeoptimizer.GloVar.Companion.minLng
import ciptateknologi.arsa.routeoptimizer.models.CentroidK
import com.google.android.gms.maps.model.LatLng
import kotlin.math.abs
import kotlin.math.pow

class KMeansClustering(val actContext : Context) {
    val jmlK = 3

    //mencari max Lat Lng terbesar dan terkecil untuk mendapatkan boundary wilayah cluster
    fun getMaxMinLatLng():Boolean{
        minLat = arrayFinalSelectedLocations[0].lat
        minLng = arrayFinalSelectedLocations[0].lng
        maxLat = minLat
        maxLng = minLng

        for(i in 0 until arrayFinalSelectedLocations.size){
            if(abs(arrayFinalSelectedLocations[i].lat)< abs(minLat)) minLat = arrayFinalSelectedLocations[i].lat
            if(abs(arrayFinalSelectedLocations[i].lng)< abs(minLng)) minLng = arrayFinalSelectedLocations[i].lng
            if(abs(arrayFinalSelectedLocations[i].lat)> abs(maxLat)) maxLat = arrayFinalSelectedLocations[i].lat
            if(abs(arrayFinalSelectedLocations[i].lng)> abs(maxLng)) maxLng = arrayFinalSelectedLocations[i].lng
        }

        arrayBoundary.clear()
        arrayBoundary.add(LatLng(minLat,minLng))  //bottom left
        arrayBoundary.add(LatLng(minLat,maxLng))  //bottom right
        arrayBoundary.add(LatLng(maxLat,maxLng))  //top right
        arrayBoundary.add(LatLng(maxLat,minLng))  //top left

//        Log.i("LatMin","$minLat")
//        Log.i("LatMax","$maxLat")
//        Log.i("LngMin","$minLng")
//        Log.i("LngMax","$maxLng")

        return true
    }

    //membuat centroid (K) awal
    fun generateRandomK(K:Int){
        var lenLat = abs(maxLat-minLat)
        var lenLng = abs(maxLng-minLng)
        var temp = 0.0

        if(lenLat>lenLng){
            lenLat = lenLat/2.0
            lenLng = lenLng/K.toDouble()
            for(i in 0 until K){
                temp = temp+lenLng
                arrayCentroidK.add(CentroidK(minLat+lenLat, minLng+temp, i))
            }
        }else{
            lenLng = lenLng/2.0
            lenLat = lenLat/K.toDouble()
            for (i in 0 until K){
                temp = temp+lenLat
                arrayCentroidK.add(CentroidK(minLat+temp, minLng+lenLng, i))
            }
        }
    }

    fun calculate(){
        var dist = 0.0
        var label : Int
        var cntLabel = 0
        var cntIterasi = 1
        var avgLat = 0.0; var avgLng = 0.0
        val arrayDist = ArrayList<Double>()

        generateRandomK(jmlK)

        while (cntIterasi<=10){
            //menyimpan nilai yg lama sebelum diganti dengan yang baru
            //metode arraylist clone kok ternyata mereference array yg lain, bukan complete copy
            arrayCentroidKOld.clear()
            for(i in 0 until arrayCentroidK.size){
                arrayCentroidKOld.add(arrayCentroidK[i].copy())
            }

            for(i in 1 until arrayFinalSelectedLocations.size){
                //hitung euclidean distance dengan setiap centroid K
                arrayDist.clear()
                for(j in 0 until arrayCentroidK.size){
                    dist = (arrayFinalSelectedLocations[i].lat - arrayCentroidK[j].lat).pow(2.0)+
                           (arrayFinalSelectedLocations[i].lng - arrayCentroidK[j].lng).pow(2.0)
                    dist = dist.pow(0.5)
                    arrayDist.add(dist)
                }

                //cek lokasi ini masuk label mana
                dist = arrayDist[0]; label = 0
                for(k in 1 until arrayDist.size){
                    if(arrayDist[k]< dist){
                        dist = arrayDist[k]
                        label = k
                    }
                }
                arrayFinalSelectedLocations[i].label = label
            }

            //hitung ulang centroid K
            for(i in 0 until arrayCentroidK.size){
                avgLat = 0.0; avgLng = 0.0; cntLabel = 0
                for(j in 1 until arrayFinalSelectedLocations.size){
                    if(arrayFinalSelectedLocations[j].label == i){
                        avgLat += arrayFinalSelectedLocations[j].lat
                        avgLng += arrayFinalSelectedLocations[j].lng
                        cntLabel++
                    }
                }

                //menambahkan my position ke setiap perhitungan centroid baru
                avgLat+= arrayFinalSelectedLocations[0].lat
                avgLng+= arrayFinalSelectedLocations[0].lng
                cntLabel++

                //hitung centroid K baru dan simpan
                arrayCentroidK[i].lat = avgLat/cntLabel.toDouble()
                arrayCentroidK[i].lng = avgLng/cntLabel.toDouble()

                Log.i("$cntIterasi . Lat-$i",
                    //"${abs(arrayCentroidKOld[i].lat-arrayCentroidK[i].lat)}")
                "${arrayCentroidK[i].lng} , ${arrayCentroidK[i].lat} --- ${arrayCentroidKOld[i].lat}")
            }
            cntIterasi++
        }
    }
}
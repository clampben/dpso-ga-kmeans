package ciptateknologi.arsa.routeoptimizer.tools

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.app.ProgressDialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.os.AsyncTask
import android.util.Log
import android.widget.Toast
import ciptateknologi.arsa.routeoptimizer.GloVar.Companion.arrDistance
import ciptateknologi.arsa.routeoptimizer.GloVar.Companion.arrDistanceOld
import ciptateknologi.arsa.routeoptimizer.GloVar.Companion.arrFitness
import ciptateknologi.arsa.routeoptimizer.GloVar.Companion.arrFitnessOld
import ciptateknologi.arsa.routeoptimizer.GloVar.Companion.arrGBest
import ciptateknologi.arsa.routeoptimizer.GloVar.Companion.arrGBest2
import ciptateknologi.arsa.routeoptimizer.GloVar.Companion.arrGBestFinal
import ciptateknologi.arsa.routeoptimizer.GloVar.Companion.arrPBest
import ciptateknologi.arsa.routeoptimizer.GloVar.Companion.arrPBestDistance
import ciptateknologi.arsa.routeoptimizer.GloVar.Companion.arrPBestDistanceOld
import ciptateknologi.arsa.routeoptimizer.GloVar.Companion.arrPBestFitness
import ciptateknologi.arsa.routeoptimizer.GloVar.Companion.arrPBestFitnessOld
import ciptateknologi.arsa.routeoptimizer.GloVar.Companion.arrPBestOld
import ciptateknologi.arsa.routeoptimizer.GloVar.Companion.arrParticleSwarm
import ciptateknologi.arsa.routeoptimizer.GloVar.Companion.arrParticleSwarmOld
import ciptateknologi.arsa.routeoptimizer.GloVar.Companion.arrSwapTemp
import ciptateknologi.arsa.routeoptimizer.GloVar.Companion.arrVelocity
import ciptateknologi.arsa.routeoptimizer.GloVar.Companion.arrVelocityOld
import ciptateknologi.arsa.routeoptimizer.GloVar.Companion.bestFitnessIndex
import ciptateknologi.arsa.routeoptimizer.GloVar.Companion.c
import ciptateknologi.arsa.routeoptimizer.GloVar.Companion.calcMethod
import ciptateknologi.arsa.routeoptimizer.GloVar.Companion.cnt
import ciptateknologi.arsa.routeoptimizer.GloVar.Companion.db
import ciptateknologi.arsa.routeoptimizer.GloVar.Companion.gBestDistance
import ciptateknologi.arsa.routeoptimizer.GloVar.Companion.gBestDistanceFinal
import ciptateknologi.arsa.routeoptimizer.GloVar.Companion.gBestDistanceOld
import ciptateknologi.arsa.routeoptimizer.GloVar.Companion.mili
import ciptateknologi.arsa.routeoptimizer.GloVar.Companion.minute
import ciptateknologi.arsa.routeoptimizer.GloVar.Companion.pBestAvg
import ciptateknologi.arsa.routeoptimizer.GloVar.Companion.qGetDistance
import ciptateknologi.arsa.routeoptimizer.GloVar.Companion.reRequestDataFromMapsProvider
import ciptateknologi.arsa.routeoptimizer.GloVar.Companion.second
import ciptateknologi.arsa.routeoptimizer.GloVar.Companion.selectedDestinationCount
import ciptateknologi.arsa.routeoptimizer.GloVar.Companion.t1
import ciptateknologi.arsa.routeoptimizer.GloVar.Companion.t2
import ciptateknologi.arsa.routeoptimizer.activities.MapActivity

import java.util.*
import kotlin.collections.ArrayList


@Suppress("DEPRECATION")
class CalcShortestRoute(val actCtx : Context) {
    var info = ""

    //main method that will be called to calculate using PSO
    fun calculate(particlesCount: Int){
        val asyncTask = AsyncTaskCalculateRoute(particlesCount)
        asyncTask.execute()
    }

    val dialogNeutral = DialogInterface.OnClickListener { dialog, which ->
        val intent = Intent(actCtx, MapActivity::class.java)
        actCtx.startActivity(intent)
    }

//    step 1 : generate random particles
    fun generateParticles(particlesCount : Int){
        if(reRequestDataFromMapsProvider){
            arrParticleSwarmOld.clear()
            val arrParticle = ArrayList<Int>()
            for (j in 0 until selectedDestinationCount) arrParticle.add(j, j + 1)

            for (i in 0 until particlesCount) { // <<===>> for(i in 0..particlesCount-1)
                for(x in 0 until 20) Collections.shuffle(arrParticle, Random(System.nanoTime()))

                //initialize some variable
                arrParticleSwarm.add(i, arrParticle.clone() as ArrayList<Int>)
                arrDistance.add(i, 0)
                arrFitness.add(i, 0.0)

                arrPBest.add(i, arrParticle.clone() as ArrayList<Int>)
                arrPBestDistance.add(i,0)
                arrPBestFitness.add(i,0.0)

                val SO = ArrayList<Int>(); SO.add(0); SO.add(0)
                val SS = ArrayList<ArrayList<Int>>(); SS.add(SO)
                arrVelocity.add(SS)
            }

            for(i in 0 until arrParticleSwarm.size){
                arrParticleSwarmOld.add(arrParticleSwarm[i].clone() as java.util.ArrayList<Int>)
                arrVelocityOld.add(arrVelocity[i].clone() as ArrayList<ArrayList<Int>>)
            }
            arrDistanceOld = arrDistance.clone() as ArrayList<Int>
            arrFitnessOld = arrFitness.clone() as ArrayList<Double>
            arrPBestOld = arrPBest.clone() as ArrayList<ArrayList<Int>>
            arrPBestDistanceOld = arrPBestDistance.clone() as ArrayList<Int>
            arrPBestFitnessOld = arrPBestFitness.clone() as ArrayList<Double>
        }else{
            for(i in 0 until arrParticleSwarmOld.size){
                arrParticleSwarm.add(arrParticleSwarmOld[i].clone() as java.util.ArrayList<Int>)
                arrVelocity.add(arrVelocityOld[i].clone() as ArrayList<ArrayList<Int>>)
            }
            arrDistance = arrDistanceOld.clone() as ArrayList<Int>
            arrFitness = arrFitnessOld.clone() as ArrayList<Double>
            arrPBest = arrPBestOld.clone() as ArrayList<ArrayList<Int>>
            arrPBestDistance = arrPBestDistanceOld.clone() as ArrayList<Int>
            arrPBestFitness = arrPBestFitnessOld.clone() as ArrayList<Double>
        }

}

//    step 2: calculate distance in each particle
    fun calculateDistanceInParticles(
        arrParticles : ArrayList<ArrayList<Int>>,
        arrDist      : ArrayList<Int>,
        arrFit       : ArrayList<Double>,
        calcAvg      : Boolean
    ){
        var node = ""
        //var tmp = 0.0

        fun getDistanceFromDb(lNode : String, i : Int){
            c = db.rawQuery(qGetDistance, arrayOf(lNode))
            //c = db.rawQuery("select dist from node_list where node like " +
            //        "'%${lNode}%'",null)
            c.moveToFirst()
            arrDist[i]+=c.getInt(0)
            c.close()
        }

        if(calcAvg){
//            tmp = pBestAvg
            pBestAvg = 0.0
        } //get average pBest //...
        arrDist.clear()

        for(i in 0 until arrParticles.size) arrDist.add(i,0)

        for(i in 0 until arrParticles.size){
            for (j in 0 until arrParticles[i].size - 1) {
                if (j.equals(0)) {
                    node = "0;${arrParticles[i][j]}"
                    getDistanceFromDb(node, i)
                }
                if (j.equals(arrParticles[i].size - 2)) {
                    node = "${arrParticles[i][j + 1]};0"
                    getDistanceFromDb(node, i)
                }

                node = "${arrParticles[i][j]};${arrParticles[i][j + 1]}"
                getDistanceFromDb(node, i)
            }
            arrFit[i] = 1.0 / arrDist[i]
            if(calcAvg) pBestAvg +=arrDist[i] //...
        }
        if(calcAvg){
//            pBestAvgOld = tmp
            pBestAvg = ((pBestAvg)/(arrParticles.size.toDouble()))
        }  //...
    }

//    step 3 : initialize GBest
    fun initializeGBest(){
        var tmp = 0.0

        for(i in 0 until arrFitness.size){
            if(arrFitness[i]>tmp){
                tmp = arrFitness[i]
                bestFitnessIndex = i
            }
        }
        arrGBest = arrParticleSwarm[bestFitnessIndex].clone() as ArrayList<Int>
        gBestDistance = arrDistance[bestFitnessIndex]
        gBestDistanceOld = gBestDistance
    }

    @SuppressLint("StaticFieldLeak")
    @Suppress("DEPRECATION")
    inner class AsyncTaskCalculateRoute(val particlesCount: Int) : AsyncTask<String,Int,String>(){
        var pg = ProgressDialog(actCtx)

        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)
            t2 = (System.nanoTime()-t1)/1000000 //nano to mili second
            mili = t2.rem(1000)
            second = (t2-mili).rem(60000)/1000
            minute = t2/60000
            db.execSQL("insert into logs(log) values(?)",arrayOf("time=;0:$minute:$second,$mili"))

            pg.dismiss()
            val dialog = AlertDialog.Builder(actCtx)
            dialog.setTitle("Info").setMessage(info).setNeutralButton("OK",dialogNeutral)
            dialog.show()
        }

        override fun onPreExecute() {
            gBestDistanceFinal = Int.MAX_VALUE
            db.execSQL("delete from logs")

            super.onPreExecute()
            pg.setCancelable(false)
            pg.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL)
            pg.setTitle("Please wait")
            pg.max = 1000
            pg.setMessage("Finding the best route. Please wait..")

            pg.show()
        }

        override fun onProgressUpdate(vararg values: Int?) {
            super.onProgressUpdate(*values)
            values[0]?.let { pg.setProgress(it) }
        }

        override fun doInBackground(vararg params: String?): String? {
            //initialize
            arrDistance.clear()
            arrParticleSwarm.clear()
            arrVelocity.clear()
            arrFitness.clear()
            arrPBest.clear()
            arrPBestDistance.clear()
            bestFitnessIndex = 0

            //calculate
            t1 = System.nanoTime()

            generateParticles(particlesCount)
            calculateDistanceInParticles(arrParticleSwarm, arrDistance, arrFitness, false)
            calculateDistanceInParticles(arrPBest, arrPBestDistance, arrPBestFitness, true)
            initializeGBest()

            if(calcMethod == 0){
                 executeGA(gBestDistance)
            }
            else executeDPSOClerc()

            if (gBestDistance< gBestDistanceFinal){
                gBestDistanceFinal = gBestDistance
                arrGBestFinal = arrGBest.clone() as ArrayList<Int>
            }

            arrGBestFinal.add(0,0)
            arrGBestFinal.add(0)

            return null
        }

        //    step 4: calculate velocity update using Clerc equation =======================================
        fun executeDPSOClerc(){
            //val c1=0.5
            //val c2=0.5
            val c1=Math.random()
            val c2=Math.random()
            var s1 = -1
            var s2 =    -1
            var tmp = 0
            var gBestOld = "Initial Global Best Particle : \n"
            var gBestNew = ""
            var txVelocity = "Velocity : \n"
            var psOld="Initial Particles Swarm : \n"
            var pBestOld=" : \n"
            var psNew=""
            var pBestNew="PBest-new : \n"
            var gBestHist = ""

            fun findSS(SS : ArrayList<ArrayList<Int>>, arrGB : ArrayList<Int> ){
                for(i in 0 until arrGB.size){
                    s1 = -1; s2 = -1
                    for(j in i until arrSwapTemp.size){
                        if(j.equals(i)){
                            if(arrGB[i] == arrSwapTemp[j]) { break }
                            else{ s1 = j }
                        }
                        else{
                            if(arrSwapTemp[j] == (arrGB[i])) {
                                s2=j
                                tmp = arrSwapTemp[s1]
                                arrSwapTemp[s1] = arrSwapTemp[s2]
                                arrSwapTemp[s2] = tmp
                            }
                        }
                    }
                    if((s1>-1) && (s2>-1) ){
                        val SO = ArrayList<Int>()
                        SO.add(s1); SO.add(s2)      //SO = swap object
                        SS.add(SO)                  //SS = swap sequence
                    }
                }
            }

            fun updatePBest(){
                for(i in 0 until arrParticleSwarm.size){
                    if(arrFitness[i]>arrPBestFitness[i]){
                        arrPBest[i] = arrParticleSwarm[i].clone() as ArrayList<Int>
                        arrPBestFitness[i] = arrFitness[i]
                        arrPBestDistance[i] = arrDistance[i]
                    }
                }
            }

            fun updateGBest(
                arrPBest : ArrayList<ArrayList<Int>>,
                arrPBestDistance : ArrayList<Int>,
            ){
                var foundNewGBest = false
                for(i in 0 until arrPBest.size){
                    if(arrPBestDistance[i]< gBestDistance){
                        gBestDistance = arrPBestDistance[i]
                        bestFitnessIndex = i
                        foundNewGBest = true
                    }
                }
                if(foundNewGBest) {
                    arrGBest = arrPBest[bestFitnessIndex].clone() as ArrayList<Int>
                    gBestDistance = arrPBestDistance[bestFitnessIndex]
                }
            }

            Log.i("GA_Calc","Clerc 1")
            var convergence = 0
            var percentage = 0.0
            var gbOld = gBestDistance
            cnt = 0
            //gBestDistanceOld = gBestDistance

            //TODO : LOG showing initial particles
            for(i in 0 until arrParticleSwarm.size){
                psOld="$psOld${i+1}. ${arrParticleSwarm[i]} = ${arrDistance[i]} m\n"
            }

            //TODO : showing initial global best
            gBestOld = "${gBestOld}${arrGBest} = ${gBestDistance} m\n"

            val batasConvergence = 20
            while (convergence <= batasConvergence){ //stop iteration when there's no change in gbest

                //find new velocity for particle(x)
                for(k in 0 until arrPBest.size){
                    if(!arrPBest[k].toString().equals(arrGBest.toString())){

                        arrSwapTemp = arrPBest[k].clone() as ArrayList<Int>
                        val SS = ArrayList<ArrayList<Int>>()

                        //1.(GBest-Pbest)
                        findSS(SS, arrGBest)    //output berupa SS

                        //2. 0.5*(GBest-PBest)
                        tmp = Math.round(0.5*SS.size).toInt() //ceil or round ?
                        for (i in tmp until SS.size) SS.removeAt(tmp)

                        //3.(PBest+Step2)
                        arrSwapTemp = arrPBest[k].clone() as ArrayList<Int>
                        for(i in 0 until SS.size){
                            tmp = arrSwapTemp[SS[i][0]]
                            arrSwapTemp[SS[i][0]] = arrSwapTemp[SS[i][1]]
                            arrSwapTemp[SS[i][1]] = tmp
                        }

                        //4. Step3-x(t)
                        arrGBest2 = arrSwapTemp.clone() as ArrayList<Int>
                        arrSwapTemp = arrParticleSwarm[k].clone() as ArrayList<Int>
                        SS.clear()
                        findSS(SS, arrGBest2) //output berupa SS

                        //5. c2*Step4
                        tmp = Math.round(c2*SS.size).toInt() //ceil or round ?
                        for (i in tmp until SS.size) SS.removeAt(tmp)

                        //6. c1*v(t)+Step5
                        if(arrVelocity[k].toString().equals("[[0, 0]]")){               //if t=1
                            arrVelocity[k] = SS.clone() as ArrayList<ArrayList<Int>>}
                        else{                                                           //if t>1
                            tmp = Math.round(c1*arrVelocity[k].size).toInt() //ceil or round ?
                            for(i in tmp until arrVelocity[k].size) arrVelocity[k].removeAt(tmp)
                            arrVelocity[k].addAll(SS)
                        }

                    }else{
                        tmp = Math.round(0.5* arrVelocity[k].size).toInt()  //ceil or round ?
                        for(i in tmp until arrVelocity[k].size) arrVelocity[k].removeAt(tmp) //0.5*v(t)
                    }
                }

                cnt++
                publishProgress(cnt)

                //TODO : log, old particle swarm, old pBest and old gBest
                gBestHist = "$gBestHist$cnt. $arrGBest = $gBestDistance m \n"

                //update x(t+1) berdasarkan v(t+1) (transposisi)
                for(i in 0 until arrVelocity.size){
                    for(j in 0 until arrVelocity[i].size){      //swap squence - i
                        tmp = arrParticleSwarm[i][arrVelocity[i][j][0]]
                        arrParticleSwarm[i][arrVelocity[i][j][0]]=arrParticleSwarm[i][arrVelocity[i][j][1]]
                        arrParticleSwarm[i][arrVelocity[i][j][1]]=tmp
                    }
                }

                //recalculate distance(t+1)
                calculateDistanceInParticles(arrParticleSwarm, arrDistance, arrFitness, false)         //new x(t+1)

                //update pBest
                updatePBest()

                calculateDistanceInParticles(arrPBest, arrPBestDistance, arrPBestFitness, true)       //old pBest

                //update gBest
                updateGBest(arrPBest, arrPBestDistance)

                //check convergence
                if(gBestDistance.equals(gbOld)){
                    convergence++
                    if(calcMethod.equals(2)) { //jika dipiliah metode kalkulasi DPSO-GA
                        if (convergence > batasConvergence) executeGA(gbOld)
                    }
                }
                if(gBestDistance<gbOld){
                    gbOld = gBestDistance
                    convergence = 0
                }

                //percentage = (1.0*gBestDistance)/(pBestAvg*1.0)
                //info = "$info konvergen = ${percentage}%\n"
                db.execSQL("insert into logs(log) values(?)",
                    arrayOf("$cnt;pso-GB;${arrGBest};${gBestDistance}"))

                //TODO : text for log
                psNew = ""
                txVelocity=""
                gBestNew = "${arrGBest} = $gBestDistance m\n"
                for(i in 0 until arrParticleSwarm.size){
                    psNew="${psNew}${i+1}. ${arrParticleSwarm[i]} = ${arrDistance[i]} m\n"
                    txVelocity="${txVelocity}${i+1}. ${arrVelocity[i]}\n" //TODO : log
//                    pBestNew="$pBestNew${arrPBest[i]}, ${arrPBestDistance[i]}, ${arrPBestFitness[i]}\n"
                }

                //info = "$info$cnt . $gBestOld | $gBestNew\n"
            }

            //TODO : LOG
            //info = "$info$psOld\n$pBestOld\n\n\n$psNew\n$pBestNew\n\n$gBestOld\n\n$gBestNew\n\n$gBestHist"
            info = "${info}\n\n${psOld}\n\n" +
                    "${gBestOld}\n\n" +
                    "Final Particles Swarm : \n${psNew}\n\n" +
                    "Final Global Best Particle : \n${gBestNew}\n\n" +
                    "Final Velocity : \n${txVelocity}\n\n"+
                    "Global Best History : \n${gBestHist}"
        }


        //metode Algoritma Genetika
        fun executeGA(extGbOld : Int){
            //TODO : Kerjakan algen disini
            Log.i("GA_1", "Begin GA")
            //Genetic Algorithm (GA)
            //======================
            //1. Penentuan parent awal
            //----------------------------------------------
            //a : ambil nilai gBest sebagai parent-1
            //b : ambil dari pBest sebagai parent-2 (cari nilai terbaik kedua setelah gBest)
            //c : copy arrPBest ke populasi GA
            var arrPopulasiGA = arrPBest.clone() as ArrayList<ArrayList<Int>>
            var arrPopulasiGADistance = arrPBestDistance.clone() as ArrayList<Int>
            var arrPopulasiGAFitness = arrPBestFitness.clone() as ArrayList<Double>
            var arrProtoChildDanMutasiGA = ArrayList<ArrayList<Int>>()    //menampung populasi anak hasil cross dan mutasi
            var arrProtoChildDanMutasiGADistance = ArrayList<Int>()
            var arrProtoChildDanMutasiGAFitness = ArrayList<Double>()
            var parent1 = ArrayList<Int>()
            var parent2 = ArrayList<Int>()
            var protoChild1 = ArrayList<Int>()
            var protoChild2 = ArrayList<Int>()
            var tmpParent2 = Int.MAX_VALUE
            var tmpX = 0
            var tmp = 0
            var jmlGenX = 0  //menentukan jumlah gen dalam kromosom yg akan disilangkan
            var startPosGenX = 0 //menentukan posisi awal kromosom yg akan disilangkan
            var stopPosGenX = 0 //menentukan posisi akhir kromosom yg akan disilangkan
            var idx = 0
            var chromosomeSize = 0
            var calcGA = true
            var generasiGA = 0
            var convergence = 0
            var convergenceMax = 20
            var gbOld = extGbOld
            val chance = 0.5

            //update global Best
            //==================
            fun updateGBest(
                arrPBest : ArrayList<ArrayList<Int>>,
                arrPBestDistance : ArrayList<Int>,
            ){
                var foundNewGBest = false
                for(i in 0 until arrPBest.size){
                    if(arrPBestDistance[i]< gBestDistance){
                        gBestDistance = arrPBestDistance[i]
                        bestFitnessIndex = i
                        foundNewGBest = true
                    }
                }
                if(foundNewGBest) {
                    arrGBest = arrPBest[bestFitnessIndex].clone() as ArrayList<Int>
                    gBestDistance = arrPBestDistance[bestFitnessIndex]
                }
            }

            //proses elitism
            //==============
            fun elitism(){
                Log.i("GA_6", "add population")
                for (i in 0 until arrProtoChildDanMutasiGA.size) {
                    arrPopulasiGA.add(arrProtoChildDanMutasiGA[i].clone() as ArrayList<Int>)
                }
                arrPopulasiGADistance.addAll(arrProtoChildDanMutasiGADistance)
                arrPopulasiGAFitness.addAll(arrProtoChildDanMutasiGAFitness)

                arrProtoChildDanMutasiGA.clear()
                arrProtoChildDanMutasiGADistance.clear()
                arrProtoChildDanMutasiGAFitness.clear()
                parent1.clear()
                parent2.clear()
                protoChild1.clear()
                protoChild2.clear()

                if (convergence != (convergenceMax)) {
                    //6. Mulai prosedur elitism
                    //-------------------------
                    //hitung jumlah kromosom / partikel yang akan dibuang
                    Log.i("GA_7", "elitism")
                    var jmlHapus = arrPopulasiGA.size - arrPBest.size
                    for (i in 0 until jmlHapus) {
                        tmp = 0; idx = 0
                        for (j in 0 until arrPopulasiGA.size) {
                            if (arrPopulasiGADistance[j] > tmp) {
                                tmp = arrPopulasiGADistance[j]
                                idx = j
                            }
                        }
                        arrPopulasiGA.removeAt(idx)
                        arrPopulasiGADistance.removeAt(idx)
                        arrPopulasiGAFitness.removeAt(idx)
                    }

                    //7. pilih ulang parent
                    //-----------------------
                    //a. parent1
                    Log.i("GA_8", "pilih parent")
                    idx = 0; tmp = Int.MAX_VALUE
                    for (i in 0 until arrPopulasiGA.size) {
                        if (arrPopulasiGADistance[i] < tmp) {
                            tmp = arrPopulasiGADistance[i]
                            idx = i
                        }
                    }

                    parent1 = arrPopulasiGA[idx].clone() as ArrayList<Int>
                    Log.i("GA_8a", "p1 = ${parent1}")

                    //b. parent2
                    tmp = Int.MAX_VALUE
                    for (i in 0 until arrPopulasiGA.size) {
                        if (arrPopulasiGADistance[i] > arrPopulasiGADistance[idx]) {
                            if (tmp > arrPopulasiGADistance[i]) {
                                tmp = arrPopulasiGADistance[i]
                                tmpX = i
                            }
                        }
                    }
                    parent2 = arrPopulasiGA[tmpX].clone() as ArrayList<Int>
                    Log.i("GA_8b", "p2 = ${parent2}")

                    Log.i("GA_9", "tampil info")
//                    if (convergence.equals(convergenceMax - 1)) {
//                        info = "Populasi GA\nGenerasi = ${generasiGA}\n"
//                        for (i in 0 until arrPopulasiGA.size) {
//                            info =
//                                "$info ${i + 1}.${arrPopulasiGA[i]} = ${arrPopulasiGADistance[i]}\n"
//                        }
//                        Log.i("GA_9a", "info ditampilkan")
//                    }
                } else {
                    arrPopulasiGA.clear()
                    arrPopulasiGADistance.clear()
                    arrPopulasiGAFitness.clear()
                }
            }



            for (i in 0 until arrDistance.size) {
                if (arrDistance[i] > gBestDistance) {
                    if (tmpParent2 > arrDistance[i]) {
                        tmpParent2 = arrDistance[i]
                        idx = i
                    }
                }
            }
            parent2 = arrParticleSwarm[idx].clone() as ArrayList<Int>
            parent1 = arrGBest.clone() as ArrayList<Int>
            chromosomeSize = parent1.size - 1

            if(calcMethod == 0){
                info = "Initialization\n"
                for(i in 0 until arrParticleSwarm.size)
                    info="$info${i+1}. ${arrParticleSwarm[i]} = ${arrDistance[i]}\n"
                info="${info}\nParent 1 = ${parent1}\nParent 2 = ${parent2}\nMutation Rate = ${chance}\n\n"
            }

            while (calcGA.and(convergence < convergenceMax)) {
                generasiGA++
                if(calcMethod == 0) publishProgress(generasiGA)
                //Log.i("GA_3", "${generasiGA}")

                //2. memulai crossover metode PMX
                //-------------------------------
                protoChild1 =
                    parent1.clone() as ArrayList<Int> //inisiasi protochild1
                protoChild2 =
                    parent2.clone() as ArrayList<Int> //inisiasi protochild2
                jmlGenX =
                    parent1.size / 2   //menentukan jumlah gen dalam kromosom yg akan disilangkan


                startPosGenX = Math.round(Math.random() * ((chromosomeSize) * 1.0))
                    .toInt() //menentukan posisi awal kromosom yg akan disilangkan
                if ((startPosGenX + jmlGenX) > (chromosomeSize)) startPosGenX =
                    startPosGenX - jmlGenX
                stopPosGenX = startPosGenX + jmlGenX
                Log.i(
                    "GA_3a",
                    "pc1=${protoChild1}, pc2=${protoChild2}, start=${startPosGenX}, stop=${stopPosGenX}"
                )

                //tukar substring antar induk
                Log.i("GA_3b", "tukar substring")
                for (i in startPosGenX..stopPosGenX) {
                    tmpX = protoChild1[i]
                    protoChild1[i] = protoChild2[i]
                    protoChild2[i] = tmpX
                }
                Log.i("GA_3b", "pc1=${protoChild1}, pc2=${protoChild2}")

                //mapping gen diluar substring
                Log.i("GA_3c", "mapping gen")
                for (i in 0..chromosomeSize) {
                    when (i) {
                        !in startPosGenX..stopPosGenX -> {
                            for (k in 0..jmlGenX) {
                                for (j in startPosGenX..stopPosGenX) {
                                    try {
                                        if (protoChild1[i] == protoChild1[j]) {
                                            tmpX = protoChild2[j]
                                            protoChild1[i] = tmpX
                                        }
                                        if (protoChild2[i] == protoChild2[j]) {
                                            tmpX = protoChild1[j]
                                            protoChild2[i] = tmpX
                                        }
                                    } catch (e: Exception) {
                                        Toast.makeText(
                                            actCtx,
                                            "${e.printStackTrace().toString()}",
                                            Toast.LENGTH_LONG
                                        ).show()
                                    }
                                }
                            }
                        }
                    }
                }

                //menampung new offspring ke array
                arrProtoChildDanMutasiGA.add(protoChild1)
                arrProtoChildDanMutasiGA.add(protoChild2)
                Log.i(
                    "GA_3d", "pc1 = ${arrProtoChildDanMutasiGA[0]}, " +
                            "pc2 = ${arrProtoChildDanMutasiGA[1]}"
                )

                //3. memulai metode mutasi (chance mutasi sebesar 10%)
                //----------------------------------------------------
                Log.i("GA_3e", "chance mutasi")

                if (Math.random() <= chance) { //mutasi terjadi pada 10% dari total gen pada populasi + protochild
                    val jmlGenTotal = (arrPopulasiGA.size * chromosomeSize) * 1.0
                    val jmlGenMutasi =
                        Math.round(jmlGenTotal * chance)  //jumlah total gen yg mutasi
                    var arrPosGenMutasi =
                        ArrayList<Int>()          //menampung posisi gen yg mutasi

                    var posGen = 0
                    var posGenMod = 0
                    var posChromosome = 0
                    for (i in 0 until jmlGenMutasi) { //menentukan gen mana saja yg mutasi, dipilih secara random
                        posGen = (Math.random() * jmlGenTotal).toInt()
                        posGenMod = posGen % chromosomeSize
                        arrPosGenMutasi.add(posGen)
                        posChromosome = posGen / chromosomeSize
                        arrProtoChildDanMutasiGA.add(arrPopulasiGA[posChromosome]) //pilih parent dari child yg akan mutasi ke array
                        posChromosome =
                            arrProtoChildDanMutasiGA.size - 1 //posisi parent didalam array yg akan mutasi
                        when (posGenMod) {    //operasi mutasi gen menggunakan metode swap
                            0 -> { //posisi gen diawal chromosome
                                tmpX = arrProtoChildDanMutasiGA[posChromosome][0]
                                arrProtoChildDanMutasiGA[posChromosome][0] =
                                    arrProtoChildDanMutasiGA[posChromosome][1]
                                arrProtoChildDanMutasiGA[posChromosome][1] = tmpX
                            }
                            (chromosomeSize - 1) -> { //posisi gen diakhir chromosome
                                tmpX =
                                    arrProtoChildDanMutasiGA[posChromosome][chromosomeSize - 1]
                                arrProtoChildDanMutasiGA[posChromosome][chromosomeSize - 1] =
                                    arrProtoChildDanMutasiGA[posChromosome][chromosomeSize - 2]
                                arrProtoChildDanMutasiGA[posChromosome][chromosomeSize - 2] =
                                    tmpX
                            }
                            else -> {
                                tmpX =
                                    arrProtoChildDanMutasiGA[posChromosome][posGenMod]
                                arrProtoChildDanMutasiGA[posChromosome][posGenMod] =
                                    arrProtoChildDanMutasiGA[posChromosome][posGenMod + 1]
                                arrProtoChildDanMutasiGA[posChromosome][posGenMod + 1] =
                                    tmpX
                            }
                        }
                    }
                }

                //4. Hitung Distance dan Fitness Offspring
                //----------------------------------------
                Log.i("GA_3f", "process")
                for (i in 0 until arrProtoChildDanMutasiGA.size) {
                    arrProtoChildDanMutasiGADistance.add(0)
                    arrProtoChildDanMutasiGAFitness.add(0.0)
                }

                Log.i("GA_4", "calculate distance")
                calculateDistanceInParticles(
                    arrProtoChildDanMutasiGA,
                    arrProtoChildDanMutasiGADistance,
                    arrProtoChildDanMutasiGAFitness,
                    false
                )
                Log.i(
                    "GA_4a",
                    "pc1 = ${arrProtoChildDanMutasiGA[0]} = ${arrProtoChildDanMutasiGADistance[0]}, " +
                            "pc2 = ${arrProtoChildDanMutasiGA[1]} = ${arrProtoChildDanMutasiGADistance[1]}"
                )


                //5. Update/Cek GBest
                //-------------------
                Log.i("GA_5", "update gbest")
                updateGBest(
                    arrProtoChildDanMutasiGA,
                    arrProtoChildDanMutasiGADistance
                )

                if(gBestDistance<gbOld){
                    Log.i("GA_5a", "found new gbest gen-${generasiGA} ")
                    Log.i("GA_5a", "gBest = ${arrGBest}, dist = ${gBestDistance}")

                    if(calcMethod != 0){ calcGA = false }
                    else {
                        convergence = 0
                        gbOld = gBestDistance
                        elitism()
                    }
                }else{
                    convergence++
                    elitism()
                }

                Log.i("GA_10", "NEXT WHILE (${convergence})================================")
                db.execSQL("insert into logs(log) values(?)",
                    arrayOf("$generasiGA;ga-GB;${arrGBest};${gBestDistance}"))
                if(calcMethod == 0) info="${info}${generasiGA}. ${arrGBest} = ${gBestDistance}\n"
            }


            arrProtoChildDanMutasiGA.clear()
            arrPopulasiGADistance.clear()
            arrPopulasiGAFitness.clear()
            arrProtoChildDanMutasiGADistance.clear()
            arrProtoChildDanMutasiGAFitness.clear()
            parent1.clear()
            parent2.clear()
            protoChild1.clear()
            protoChild2.clear()
        }
    }
}
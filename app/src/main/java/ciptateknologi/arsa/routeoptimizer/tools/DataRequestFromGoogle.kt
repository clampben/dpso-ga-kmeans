package ciptateknologi.arsa.routeoptimizer.tools

import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.widget.Toast
import com.android.volley.Request
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import ciptateknologi.arsa.routeoptimizer.InfoLokasi
import ciptateknologi.arsa.routeoptimizer.GloVar.Companion.arrayFinalSelectedLocations
import ciptateknologi.arsa.routeoptimizer.GloVar.Companion.arrayLocations
import ciptateknologi.arsa.routeoptimizer.GloVar.Companion.db
import ciptateknologi.arsa.routeoptimizer.GloVar.Companion.nodesCount
import ciptateknologi.arsa.routeoptimizer.GloVar.Companion.nodesCounter
import ciptateknologi.arsa.routeoptimizer.GloVar.Companion.selectedDestinationCount
import org.json.JSONException

class DataRequestFromGoogle(val actCtx : Context){

    val MAPBOX_TOKEN = "pk.eyJ1IjoiYmVubmludWdyb2hvIiwiYSI6ImNrMWh6Z2FnaTBjYmozYm9jcndicGQ5eDMifQ.nXsgeOO1zbZ-F0dbYtC_lw"
    var url1=
        "https://maps.googleapis.com/maps/api/place/textsearch/json?" +
                "key=AIzaSyDSnmWTWDD6c89RGqZ1cDnJxCHiBmqO2C8&query="
    var url=""

    fun getLocationsList(locationName : String, lat : Double, lng : Double){
        url = url1 + Uri.encode(locationName)+ "&location=$lat,$lng"

        val request = JsonObjectRequest(Request.Method.GET,url,null,
            {
                arrayLocations.clear()

                val jsonArray = it.getJSONArray("results")
                for(i in 0 .. (jsonArray.length()-1)){
                    val jsonObject = jsonArray.getJSONObject(i)
                    val f_address = jsonObject.getString("formatted_address")
                    val name = jsonObject.getString("name")
                    val place_id = jsonObject.getString("place_id")
                    val geometry = jsonObject.getJSONObject("geometry")
                    val location = geometry.getJSONObject("location")
                    val lat = location.getDouble("lat")
                    val lng = location.getDouble("lng")
                    arrayLocations.add(InfoLokasi(name,f_address,lat,lng,place_id,false,0))
                }
                val intent = Intent("places_ready")
                actCtx.sendBroadcast(intent)

            },
            {
                Toast.makeText(actCtx, it.message, Toast.LENGTH_SHORT).show()
            }
        )
        val q = Volley.newRequestQueue(actCtx)
        q.add(request)
    }

    //mendapatkan jarak antar dua lokasi
    fun getDistanceBetweenTwoPlacesMapbox(x: Int, y: Int, reverse : Boolean, pgd : ProgressDialog){
        var i = x
        var j = y

        if(reverse){
            val temp = i
            i = j
            j = temp
        }

        url =
            "https://api.mapbox.com/directions/v5/mapbox/driving/" +
                    "${arrayFinalSelectedLocations[i].lng},${arrayFinalSelectedLocations[i].lat};" +
                    "${arrayFinalSelectedLocations[j].lng},${arrayFinalSelectedLocations[j].lat}?" +
                    "access_token=$MAPBOX_TOKEN&geometries=geojson"

        val request = JsonObjectRequest(Request.Method.GET,url,null,
            {
                try {
                    var path = ""
                    val routes = it.getJSONArray("routes").getJSONObject(0)
                    val duration = routes.getDouble("duration")
                    val legs = routes.getJSONArray("legs").getJSONObject(0)
                    val distance = legs.getDouble("distance")
                    val geometry = routes.getJSONObject("geometry")
                    val coordinates = geometry.getJSONArray("coordinates")
                    for(i in 0..coordinates.length()-1){
                        val lngLat = coordinates.getJSONArray(i)
                        val fLng = lngLat.getDouble(0)
                        val fLat = lngLat.getDouble(1)
                        path="$path$fLat;$fLng;"
                    }
                    path=path.substring(0,path.length)

                    db.execSQL("insert into node_list(node, dist, points, srcloc, dstloc, duration) values(?,?,?,?,?,?)",
                        arrayOf("$i;$j",distance,path,arrayFinalSelectedLocations[i].namaTempat,
                            arrayFinalSelectedLocations[j].namaTempat,duration))

                    nodesCounter += 1
                } catch (e: JSONException) {
                    Toast.makeText(actCtx,it.toString(),Toast.LENGTH_LONG).show()
                    pgd.dismiss()
                }

                //jumlah partikel 2x dari jumlah lokasi dipilih
                if(nodesCounter.equals(nodesCount)){
                    pgd.dismiss()
                    val calcShortestRoute = CalcShortestRoute(actCtx)
                    calcShortestRoute.calculate(selectedDestinationCount*2)
                }
            },
            {
                Toast.makeText(actCtx, "${it.message}", Toast.LENGTH_SHORT).show()
                pgd.dismiss()
            })
        val q = Volley.newRequestQueue(actCtx)
        q.add(request)
    }

}
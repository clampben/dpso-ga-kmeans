package ciptateknologi.arsa.routeoptimizer

data class InfoLokasi(
    val namaTempat : String,
    val alamat : String,
    val lat : Double,
    val lng : Double,
    val placeId : String,
    var dipilih : Boolean,
    var label : Int)
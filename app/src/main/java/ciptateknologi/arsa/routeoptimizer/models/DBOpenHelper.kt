package id.ac.poltek_kediri.informatika.travellingbestroute

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

class DBOpenHelper(context : Context):
    SQLiteOpenHelper(context, dbName, null, version) {

    override fun onCreate(db: SQLiteDatabase?) {
        val sql = "create table node_list(node text, srcloc string, dstloc string, dist double, duration double, points string)"            //store distance between locations / nodes
        val sql2 = "create table logs(log text)"
        db?.execSQL(sql)
        db?.execSQL(sql2)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
    }

    companion object{
        val dbName="db_node"
        val version=1
    }
}
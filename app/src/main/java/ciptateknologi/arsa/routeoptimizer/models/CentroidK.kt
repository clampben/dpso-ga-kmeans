package ciptateknologi.arsa.routeoptimizer.models

data class CentroidK (
    var lat : Double,
    var lng : Double,
    var label : Int
)
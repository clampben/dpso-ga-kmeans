package ciptateknologi.arsa.routeoptimizer

import android.app.Application
import android.app.ProgressDialog
import android.content.ContentValues
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.Polyline
import ciptateknologi.arsa.routeoptimizer.adapters.PolylinesAdapter
import ciptateknologi.arsa.routeoptimizer.models.CentroidK
import com.google.android.gms.maps.model.LatLng

class GloVar : Application() {
    companion object{
        var arrayCentroidK : ArrayList<CentroidK> = ArrayList()
        var arrayCentroidKOld : ArrayList<CentroidK> = ArrayList()
        var arrayBoundary = ArrayList<LatLng>()
        var arrayMarker = ArrayList<Marker>()
        var arrayLocations : ArrayList<InfoLokasi> = ArrayList()
        var arraySelectedLocations : ArrayList<InfoLokasi> = ArrayList()
        var arrayFinalSelectedLocations : ArrayList<InfoLokasi> = ArrayList()
        var selectedDestinationCount = 0
        //var expectedNodesCount = 0
        var nodesCount = 0
        var nodesCounter = 0
        var lat = 0.0
        var lng = 0.0
        lateinit var db : SQLiteDatabase
        lateinit var cv : ContentValues
        lateinit var c : Cursor
        var scale = 0f
        var cnt = 0

        var arrParticleSwarm : ArrayList<ArrayList<Int>> = ArrayList()  //store particle swarm
        var arrVelocity : ArrayList<ArrayList<ArrayList<Int>>> = ArrayList()               //store velocity of the particles
        var arrDistance : ArrayList<Int> = ArrayList()                  //store distance of each particle
        var arrFitness  : ArrayList<Double> = ArrayList()               //store fitness of each particle

        var arrPBest : ArrayList<ArrayList<Int>> = ArrayList()          //store Pbest of each particle
        var arrPBestDistance : ArrayList<Int> = ArrayList()             //store Pbest distance of each particle
        var arrPBestFitness : ArrayList<Double> = ArrayList()
        var arrGBest : ArrayList<Int> = ArrayList()                     //store Gbest of the particle
        var arrGBest2 : ArrayList<Int> = ArrayList()
        var arrGBestFinal : ArrayList<Int> = ArrayList()                //route as a result stored in here
        var gBestDistanceFinal = 0
        var gBestDistanceOld = 0
        var gBestDistanceOldGa = 0
        var gBestDistance = 0                                           //store distance of gBest
        var pBestAvg = 0.0
        //var pBestAvgOld = 0.0
        var bestFitnessIndex = 0                                       //store index of the best fitness
        var arrSwapTemp : ArrayList<Int> = ArrayList()

        var arrParticleSwarmOld : ArrayList<ArrayList<Int>> = ArrayList()  //store particle swarm
        var arrDistanceOld : ArrayList<Int> = ArrayList()                  //store distance of each particle
        var arrFitnessOld  : ArrayList<Double> = ArrayList()               //store fitness of each particle
        var arrVelocityOld : ArrayList<ArrayList<ArrayList<Int>>> = ArrayList()               //store velocity of the particles
        var arrPBestOld : ArrayList<ArrayList<Int>> = ArrayList()          //store Pbest of each particle
        var arrPBestDistanceOld : ArrayList<Int> = ArrayList()             //store Pbest distance of each particle
        var arrPBestFitnessOld : ArrayList<Double> = ArrayList()

        var qGetDistance = "select dist from node_list where node=?"
        var reRequestDataFromMapsProvider = true

        var t1 : Long = 0
        var t2 : Long = 0
        var mili : Long = 0
        var second : Long = 0
        var minute : Long = 0
        var hour : Long = 0
        var day : Long = 0
        var oldSearch = ""
        var calcMethod = 2 //0 = GA, 1 = DPSO, 2 = DPSO-GA
        var maxLng = 0.0
        var maxLat = 0.0
        var minLng = 0.0
        var minLat = 0.0

        lateinit var polylineHighlightThin : Polyline
        lateinit var polylineHighlightBold : Polyline
        lateinit var adapter: PolylinesAdapter
        var polylineReady = false

        lateinit var pg : ProgressDialog
        var screenHeightPx = 0
        var screenWidthPx = 0
    }
}
package ciptateknologi.arsa.routeoptimizer.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import ciptateknologi.arsa.routeoptimizer.InfoLokasi
import ciptateknologi.arsa.routeoptimizer.R

class CustomAdapter(val placesList : ArrayList<InfoLokasi>) : RecyclerView.Adapter<CustomAdapter.ViewHolder>() {

    inner class ViewHolder(v : View) : RecyclerView.ViewHolder(v) {
        val cb = v.findViewById<CheckBox>(R.id.cbPilih)
        val txNamaTempat = v.findViewById<TextView>(R.id.txNamaTmpt)
        val txAlamat = v.findViewById<TextView>(R.id.txAlamat)
        val txLat = v.findViewById<TextView>(R.id.txLat)
        val txLng = v.findViewById<TextView>(R.id.txLng)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_lokasi,parent,false)
        return ViewHolder(v)
    }

    override fun getItemCount(): Int {
        return placesList.size
    }

    override fun onBindViewHolder(h: ViewHolder, position: Int) {
        val data = placesList.get(position)
        h.txNamaTempat.setText(data.namaTempat)
        h.txAlamat.setText(data.alamat)
        h.txLat.setText(data.lat.toString())
        h.txLng.setText(data.lng.toString())
        h.cb.isChecked = data.dipilih
        h.cb.setOnClickListener {
            placesList.get(position).dipilih = !placesList.get(position).dipilih }
    }
}



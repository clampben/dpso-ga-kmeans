package ciptateknologi.arsa.routeoptimizer.adapters

import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.PolylineOptions
import ciptateknologi.arsa.routeoptimizer.GloVar.Companion.adapter
import ciptateknologi.arsa.routeoptimizer.GloVar.Companion.lat
import ciptateknologi.arsa.routeoptimizer.GloVar.Companion.lng
import ciptateknologi.arsa.routeoptimizer.GloVar.Companion.polylineHighlightBold
import ciptateknologi.arsa.routeoptimizer.GloVar.Companion.polylineHighlightThin
import ciptateknologi.arsa.routeoptimizer.GloVar.Companion.polylineReady
import ciptateknologi.arsa.routeoptimizer.InfoLokasi
import ciptateknologi.arsa.routeoptimizer.R
import java.util.*
import kotlin.collections.ArrayList

class PolylinesAdapter(val ctx : Context, val placesList : ArrayList<InfoLokasi>, val gMap : GoogleMap) :
    RecyclerView.Adapter<PolylinesAdapter.ViewHolder>(){

    inner class ViewHolder(v : View) : RecyclerView.ViewHolder(v) {
        val txNamaTempat = v.findViewById<TextView>(R.id.txNamaTmpt)
        val txAlamat = v.findViewById<TextView>(R.id.txAlamat)
        val cl = v.findViewById<ConstraintLayout>(R.id.clItemLokasi)
        val im = v.findViewById<ImageView>(R.id.imArrowDown)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder{
        val v = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_locations,parent,false)
        if(polylineReady == false) {
            polylineReady = true
            polylineHighlightThin = gMap.addPolyline(PolylineOptions())
            polylineHighlightBold = gMap.addPolyline(PolylineOptions())
        }
        return ViewHolder(v)
    }

    override fun getItemCount(): Int {
        return placesList.size
    }

    override fun onBindViewHolder(h: ViewHolder, position: Int) {

        h.txNamaTempat.setText(placesList[position].namaTempat)
        if(position == (placesList.size-1)) {
            h.im.visibility = View.INVISIBLE
            h.txAlamat.setText("")
        }else{
            h.im.visibility = View.VISIBLE
            h.txAlamat.setText("${placesList[position].lat.toInt()} m")
        }

        if(placesList[position].dipilih) h.cl.setBackgroundColor(Color.rgb(90,245,240))
        else h.cl.setBackgroundColor(Color.argb(0,0,0,0))

        h.cl.setOnClickListener {
            //we using data from MapBox
            val dipilih = !placesList[position].dipilih
            polylineHighlightThin.remove()
            polylineHighlightBold.remove()

            if(!position.equals(placesList.size-1)){
                for(i in 0 until placesList.size) placesList[i].dipilih = false
                placesList[position].dipilih = dipilih

                if(placesList[position].dipilih){
                    //gambar lagi boss, this is pain in the ass
                    val llMapBox : ArrayList<LatLng> = ArrayList()
                    val tokens = StringTokenizer(placesList[position].alamat,";",false)
                    while(tokens.hasMoreTokens()){
                        lat = tokens.nextToken().toDouble()
                        lng = tokens.nextToken().toDouble()
                        llMapBox.add(LatLng(lat,lng))
                    }

                    polylineHighlightBold = gMap.addPolyline(PolylineOptions()
                        .color(Color.BLUE).width(18.0f).addAll(llMapBox).zIndex(4f))
                    polylineHighlightThin = gMap.addPolyline(PolylineOptions()
                        .color(Color.GREEN).width(6.0f).addAll(llMapBox).zIndex(5f))
                    llMapBox.clear()
                }
                else{ polylineHighlightThin.remove()
                    polylineHighlightBold.remove()}
            }
            adapter.notifyDataSetChanged()
        }
    }
}
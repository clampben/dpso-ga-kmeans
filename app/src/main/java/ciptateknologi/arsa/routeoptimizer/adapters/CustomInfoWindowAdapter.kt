package ciptateknologi.arsa.routeoptimizer.adapters

import android.app.Activity
import android.content.Context
import android.view.View
import android.widget.Button
import android.widget.TextView
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.Marker
import ciptateknologi.arsa.routeoptimizer.R

class CustomInfoWindowAdapter(val context : Context, val showOptions: Boolean) : GoogleMap.InfoWindowAdapter {

    val view = (context as Activity).layoutInflater.inflate(R.layout.marker_info_window,null)

    fun infoWindowRenderer(m:Marker, v:View){
        val txLocationName  = v.findViewById<TextView>(R.id.txLocationName)
        val txLocationAddress = v.findViewById<TextView>(R.id.txLocationAddr)
        val txLocationLat = v.findViewById<TextView>(R.id.txLocationLat)
        val txLocationLng = v.findViewById<TextView>(R.id.txLocationLng)
        val btnShowOptions = v.findViewById<Button>(R.id.button)

        txLocationName.text = m.title
        txLocationAddress.text = m.snippet
        txLocationLat.text = m.position.latitude.toString()
        txLocationLng.text = m.position.longitude.toString()
        if(showOptions == false) btnShowOptions.text = "Click this info window for more info"
    }

    override fun getInfoWindow(p0: Marker): View? {
        infoWindowRenderer(p0,view)
        return view
    }

    override fun getInfoContents(p0: Marker): View? {
        infoWindowRenderer(p0,view)
        return view
    }
}